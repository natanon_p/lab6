import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
	public static void main (String [] args) throws IOException{
		/*String test = "apple banana apple orange apple banana grape grape orange pineapple banana";
		Scanner scanner = new Scanner(test);*/
		WordCounter counter = new WordCounter();
		/*while(scanner.hasNext())
			counter.addWord(scanner.next());
		System.out.println(counter.getWords());
		System.out.println(counter.getCount("apple"));
		System.out.println(counter.getCount("banana"));
		System.out.println(counter.getCount("grape"));
		System.out.println(counter.getCount("orange"));
		System.out.println(counter.getCount("pineapple"));
		System.out.println(Arrays.toString(counter.getSortedWords()));*/
		String FILE_URL = "https://bitbucket.org/skeoop/oop/raw/master/week6/Alice-in-Wonderland.txt";
		URL url = new URL( FILE_URL);
		InputStream input = url.openStream( );
		Scanner scanner = new Scanner( input );
		final String DELIMS = "[\\s,.\\?!\"():;]+";
		scanner.useDelimiter(DELIMS);
		while(scanner.hasNext())
			counter.addWord(scanner.next());
		System.out.println(counter.getWords());
		for(int i = 0;i < 20;i++)
			System.out.print(counter.getSortedWords()[i] + " ,");
		System.out.println();
			
		
	}
}
