import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class WordCounter {
	private Map<String,Integer> wordMap = new HashMap<String,Integer> ();
	public void addWord(String word){
		String newWord = word.toLowerCase();
		if(wordMap.containsKey(newWord)){
			wordMap.replace(newWord, wordMap.get(newWord)+1);
		}
		else wordMap.put(newWord, 1);
	}
	public Set<String> getWords(){
		Set<String> wordSet = wordMap.keySet();
		return wordSet;
	}
	public int getCount(String word){
		if(wordMap.containsKey(word.toLowerCase()))
			return wordMap.get(word);
		return 0;
	}
	public String[] getSortedWords(){
		List<String> list = new ArrayList<String>(getWords());
		Collections.sort(list);
		String[] temp = new String [list.size()];
		list.toArray(temp);
		return temp;
	}
}
